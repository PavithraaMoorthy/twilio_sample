//
//  AppDelegate.swift
//  Twilio Voice with CallKit Quickstart - Swift
//
//  Copyright © 2016 Twilio, Inc. All rights reserved.
//

import UIKit
import TwilioVoice
import Firebase
import FirebaseInstanceID
import FirebaseMessaging
import UserNotifications

fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l < r
    case (nil, _?):
        return true
    default:
        return false
    }
}

fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l > r
    default:
        return rhs < lhs
    }
}

let  APP =  UIApplication.shared.delegate as! AppDelegate

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate, MessagingDelegate {

    var window: UIWindow?
    
    var deviceToken:String = String()
    var stringBold = String.init()
    var notificationCount = Int()
    var fcmToken = ""

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        NSLog("Twilio Voice Version: %@", TwilioVoice.version())

        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            // For iOS 10 data message (sent via FCM
            Messaging.messaging().delegate = self
            Messaging.messaging().shouldEstablishDirectChannel = true
            Messaging.messaging().isAutoInitEnabled = true
        }
        else
        {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        application.registerForRemoteNotifications()
        FirebaseApp.configure()
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    //MARK: - RANDOM CHAT NOTIFICATIONS
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void)
    {
        print(userInfo)
        let typeKey = "notification_type"
        let Type = userInfo[typeKey] as! String
        if Type == "12"
        {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "cancelNotification"), object: nil)
        }
        else
        {
            
        }
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any])
    {
        Messaging.messaging().appDidReceiveMessage(userInfo)
        print(userInfo)
        let typeKey = "notification_type"
        let Type = userInfo[typeKey] as! String
        if Type == "12"
        {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "cancelNotification"), object: nil)
        }
        else
        {
            
        }
    }
    func application(received remoteMessage: MessagingRemoteMessage)
    {
        print("Received data message - 1: \(remoteMessage.appData)")
        
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data)
    {
        var deviceTokens = "\(NSData(data: deviceToken))"
        deviceTokens = deviceTokens.replacingOccurrences(of: "<", with: "").replacingOccurrences(of: ">", with: "").replacingOccurrences(of: " ", with: "")
        APP.deviceToken = deviceTokens
        let token =  Messaging.messaging().fcmToken
        if token != nil
        {
            APP.fcmToken = token!
            
            connectToFcm()
        }
    }
    
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String)
    {
        APP.fcmToken = fcmToken
        connectToFcm()
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error)
    {
        APP.fcmToken = "123456789"
        APP.deviceToken = "009988776655"
    }
    
    func connectToFcm()
    {
        Messaging.messaging().connect { (error) in
            if (error != nil) {
                print("Unable to connect with FCM. \(error ?? "error unreadable" as! Error)")
            } else {
                print("Connected to FCM.")
            }
        }
    }
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        let content = notification.request.content
        // Process notification content
        print("\(content.userInfo)")
//        let typeKey = "notification_type"
//        let Type = content.userInfo[typeKey] as! String
//        if Type == "12"
//        {
//            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "cancelNotification"), object: nil)
//        }
//        else
//        {
//
//        }
        completionHandler([.alert, .sound]) // Display notification as
    }
    
    
    //MARK: - SECONDS
    func seconds(from string: String) -> Bool{
        
        let currentDate = NSDate.init()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = NSTimeZone(name: "UTC")! as TimeZone
        let date = dateFormatter.date(from: string)!
        let dates = date.addingTimeInterval(40)
        
        let comparisionResult = currentDate.compare(dates)
        
        if comparisionResult == .orderedAscending
        {
            return true
        }
        else if comparisionResult == .orderedDescending
        {
            return false
        }
        else
        {
            return false
        }
    }
}

